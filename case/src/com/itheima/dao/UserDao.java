package com.itheima.dao;

import com.itheima.domain.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    void save(User user);

    void deleteById(String id);

    User findById(String id);

    void update(User user);

    User login(User u);

    User checkName(String name);
}
